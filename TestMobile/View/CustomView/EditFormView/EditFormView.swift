//
//  EditFormView.swift
//  TestMobile
//
//  Created by Huy Tong N.H. on 11/10/18.
//  Copyright © 2018 Huy Tong All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

@IBDesignable
final class EditFormView: UIView {
    
    // MARK: IBInspectable
    @IBInspectable var title: String? {
        didSet {
            titleLabel.text = title
        }
    }    
    
    // MARK: IBOutlets
    
    @IBOutlet fileprivate weak var contentView: UIView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var textField: UITextField!
    
    var text: String? {
        set {
            textField.text = newValue
        }
        get {
            return textField.text
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("EditFormView", owner: self)
        addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

extension Reactive where Base: EditFormView {
    
    var text: ControlProperty<String?> {
        return base.textField.rx.text
    }
}
