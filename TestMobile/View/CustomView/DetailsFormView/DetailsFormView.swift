//
//  DetailsFormView.swift
//  TestMobile
//
//  Created by Huy Tong N.H. on 11/10/18.
//  Copyright © 2018 Huy Tong All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

@IBDesignable
final class DetailsFormView: UIView {
    
    // MARK: IBInspectable
    @IBInspectable var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    @IBInspectable var buttonImage: UIImage? {
        didSet {
            button.setImage(buttonImage, for: .normal)
        }
    }

    // MARK: IBOutlets
    
    @IBOutlet fileprivate weak var contentView: UIView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var contentLabel: UILabel!
    @IBOutlet fileprivate weak var button: UIButton!
    
    var text: String? {
        set {
            contentLabel.text = newValue
        }
        get {
            return contentLabel.text
        }
    }
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("DetailsFormView", owner: self)
        addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

extension Reactive where Base: DetailsFormView {
    
    var text: Binder<String?> {
        return base.contentLabel.rx.text
    }
    
    var buttonTap: ControlEvent<Void> {
        return base.button.rx.tap
    }
    
}

