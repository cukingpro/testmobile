//
//  EditViewController.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxBiBinding

class EditViewController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var imageButton: UIButton!
    @IBOutlet private weak var nameView: EditFormView!
    @IBOutlet private weak var phoneView: EditFormView!
    @IBOutlet private weak var emailView: EditFormView!
    @IBOutlet private weak var addressView: EditFormView!
    @IBOutlet private weak var postalCodeView: EditFormView!
    @IBOutlet private weak var cityView: EditFormView!
    
    // MARK: Properties
    
    var viewModel: EditViewModel!
    let disposeBag = DisposeBag()

    // MARK: Lifecycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    // MARK: - Private functions
    
    private func setUpUI() {
        setUpNavigtaionBar()
        setUpData()
    }
    
    private func setUpNavigtaionBar() {
        title = "Fiche client"
        
        let barButtonItem = UIBarButtonItem()
        barButtonItem.title = "Enregister"
        navigationItem.rightBarButtonItem = barButtonItem
        barButtonItem
            .rx
            .tap
            .subscribe(onNext: { [weak self] _ in
                print("Enregister")
            })
            .disposed(by: disposeBag)
    }
    
    private func setUpData() {
        
        if let name = viewModel.customer.value.name {
            imageView.setImageForName(name, backgroundColor: .lightGray, circular: true, textAttributes: nil)
        }
        
        (nameView.rx.text <-> viewModel.name)
            .disposed(by: disposeBag)
        
        (phoneView.rx.text <-> viewModel.phone)
            .disposed(by: disposeBag)
        
        (emailView.rx.text <-> viewModel.email)
            .disposed(by: disposeBag)
        
        (addressView.rx.text <-> viewModel.address)
            .disposed(by: disposeBag)
        
        (postalCodeView.rx.text <-> viewModel.postalCode)
            .disposed(by: disposeBag)
        
        (cityView.rx.text <-> viewModel.city)
            .disposed(by: disposeBag)
    }

}
