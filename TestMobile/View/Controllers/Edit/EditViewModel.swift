//
//  EditViewModel.swift
//  TestMobile
//
//  Created by Huy Tong N.H. on 11/10/18.
//  Copyright © 2018 Huy Tong All rights reserved.
//

import RxSwift
import RxCocoa

final class EditViewModel {
    
    let customer: BehaviorRelay<Customer>
    
    let image: BehaviorRelay<UIImage?>
    let name: BehaviorRelay<String?>
    let phone: BehaviorRelay<String?>
    let email: BehaviorRelay<String?>
    let address: BehaviorRelay<String?>
    let postalCode: BehaviorRelay<String?>
    let city: BehaviorRelay<String?>
    
    init(customer: Customer) {
        self.customer = BehaviorRelay(value: customer)
        
        image = BehaviorRelay(value: nil)
        name = BehaviorRelay(value: customer.name)
        phone = BehaviorRelay(value: customer.phone)
        email = BehaviorRelay(value: customer.email)
        address = BehaviorRelay(value: customer.address)
        postalCode = BehaviorRelay(value: customer.postalCode)
        city = BehaviorRelay(value: customer.city)
    }
    
}
