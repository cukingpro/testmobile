//
//  DetailsViewController.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MessageUI
import MapKit

class DetailsViewController: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet private weak var firstNameLabel: UILabel!
    @IBOutlet private weak var lastNameLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var phoneView: DetailsFormView!
    @IBOutlet private weak var emailView: DetailsFormView!
    @IBOutlet private weak var addressView: DetailsFormView!


    var viewModel: DetailsViewModel!
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }

    // MARK: - Private functions

    private func setUpUI() {
        setUpNavigtaionBar()
        setUpData()
        setUpButtons()
    }

    private func setUpNavigtaionBar() {
        title = "Fiche client"

        let barButtonItem = UIBarButtonItem()
        barButtonItem.title = "Editer"
        navigationItem.rightBarButtonItem = barButtonItem
        barButtonItem
            .rx
            .tap
            .withLatestFrom(viewModel.customer)
            .subscribe(onNext: { [weak self] customer in
                let editVC = EditViewController()
                let editVM = EditViewModel(customer: customer)
                editVC.viewModel = editVM
                self?.navigationController?.pushViewController(editVC)
            })
            .disposed(by: disposeBag)
    }
    
    private func setUpData() {
        
        viewModel
            .customer
            .subscribe(onNext: { [weak self] customer in
                self?.setUp(with: customer)
            })
            .disposed(by: disposeBag)
    }
    
    private func setUpButtons() {
        phoneView
            .rx
            .buttonTap
            .asObservable()
            .withLatestFrom(viewModel.customer)
            .subscribe(onNext: { [weak self] customer in
                guard let phone = customer.phone else {
                    self?.showAlert(title: "Error", message: "Phone is empty")
                    return
                }
                self?.openPhone(phone: phone)
            })
            .disposed(by: disposeBag)
        
        emailView
            .rx
            .buttonTap
            .asObservable()
            .withLatestFrom(viewModel.customer)
            .subscribe(onNext: { [weak self] customer in
                guard let email = customer.email else {
                    self?.showAlert(title: "Error", message: "Emmail is empty")
                    return
                }
                
                self?.openMail(email: email)
            })
            .disposed(by: disposeBag)
        
        addressView
            .rx
            .buttonTap
            .asObservable()
            .withLatestFrom(viewModel.customer)
            .subscribe(onNext: { [weak self] customer in
                guard let this = self else { return }
                guard let address = customer.address else {
                    this.showAlert(title: "Error", message: "Address is empty")
                    return
                }
                this.coordinates(forAddress: address) {
                    (location) in
                    guard let location = location else {
                        this.showAlert(title: "Error", message: "Can't find location")
                        return
                    }
                    this.openMapForPlace(lat: location.latitude, long: location.longitude)
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func setUp(with customer: Customer) {
        if let name = customer.name {
            imageView.setImageForName(name, backgroundColor: .lightGray, circular: true, textAttributes: nil)
        }
        firstNameLabel.text = customer.firstName
        lastNameLabel.text = customer.lastName
        phoneView.text = customer.phone
        emailView.text = customer.email
        addressView.text = customer.address
    }
}

extension DetailsViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}

// MARK: Button handle

extension DetailsViewController {
    
    private func coordinates(forAddress address: String, completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) {
            (placemarks, error) in
            guard error == nil else {
                print("Geocoding error: \(error!)")
                completion(nil)
                return
            }
            completion(placemarks?.first?.location?.coordinate)
        }
    }
    
    private func openMapForPlace(lat:Double = 0, long:Double = 0, placeName:String = "") {
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = long
        
        let regionDistance:CLLocationDistance = 100
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = placeName
        mapItem.openInMaps(launchOptions: options)
    }
    
    private func openPhone(phone: String) {
        guard let url = URL(string: "tel://\(phone)") else {
            showAlert(title: "Error", message: "Invalid phone")
            return
        }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            showAlert(title: "Error", message: "Device doesn't support phone call")
        }
    }
    
    private func openMail(email: String) {
        guard email.isEmail else {
            showAlert(title: "Error", message: "Invalid email")
            return
        }
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setSubject("Hello!")
            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            showAlert(title: "Error", message: "Device doesn't support email")
        }

    }
}
