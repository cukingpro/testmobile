//
//  DetailsViewModel.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import RxSwift
import RxCocoa

final class DetailsViewModel {

    let customer: BehaviorRelay<Customer>

    init(customer: Customer) {
        self.customer = BehaviorRelay(value: customer)
    }

}
