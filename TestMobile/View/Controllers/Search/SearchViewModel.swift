//
//  SearchViewModel.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import RxSwift
import RxCocoa

final class SearchViewModel {

    let disposeBag = DisposeBag()
    
    let customers: BehaviorRelay<[Customer]> = BehaviorRelay(value: [])
    
    func getCustomers() {
        CustomerService
            .getCustomer()
            .asObservable()
            .bind(to: customers)
            .disposed(by: disposeBag)
    }

}
