//
//  SearchViewController.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import UIKit
import SwifterSwift
import RxSwift
import RxCocoa

class SearchViewController: UIViewController {

    // MARK: IBOutlets

    @IBOutlet weak var seachBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!

    let viewModel = SearchViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.getCustomers()
    }
    
    // MARK: - Private functions

    private func setUpUI() {
        setUpNavigtaionBar()
        setUpSearchBar()
        setUpTableView()
    }

    private func setUpNavigtaionBar() {
        title = "Annuare"
    }

    private func setUpSearchBar() {

    }

    private func setUpTableView() {
        tableView.register(nibWithCellClass: CustomerTableViewCell.self)
        tableView.tableFooterView = UIView()

        viewModel.customers
            .bind(to: tableView.rx.items) { tableView, row, customer in
                let indexPath = IndexPath(row: row, section: 0)
                let cell = tableView.dequeueReusableCell(withClass: CustomerTableViewCell.self, for: indexPath)
                cell.setUp(with: customer)
                return cell
            }
            .disposed(by: disposeBag)

        tableView
            .rx
            .modelSelected(Customer.self)
            .subscribe(onNext: { [weak self] customer in
                let detailsVC = DetailsViewController()
                let detailsVM = DetailsViewModel(customer: customer)
                detailsVC.viewModel = detailsVM
                self?.navigationController?.pushViewController(detailsVC)
            })
            .disposed(by: disposeBag)
    }

}

