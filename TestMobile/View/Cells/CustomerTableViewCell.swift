//
//  CustomerTableViewCell.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import UIKit
import InitialsImageView

class CustomerTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var firstNameLabel: UILabel!
    @IBOutlet private weak var lastNameLabel: UILabel!
    @IBOutlet private weak var avatarImageView: UIImageView!
    
    func setUp(with customer: Customer) {
        if let name = customer.name {
            avatarImageView.setImageForName(name, backgroundColor: .lightGray, circular: true, textAttributes: nil)
        }
        firstNameLabel.text = customer.firstName
        lastNameLabel.text = customer.lastName
    }
}
