//
//  AppDelegate.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SwiftyUserDefaults
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let disposeBag = DisposeBag()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)

        let searchVC = SearchViewController()
        let navi = UINavigationController(rootViewController: searchVC)
        navi.navigationBar.setColors(background: .darkGray, text: .white)
        window?.rootViewController = navi
        window?.makeKeyAndVisible()
        
        setUpKeyboardManager()
        
        CustomerService.logIn().subscribeOn(MainScheduler.instance).subscribe(
            onSuccess: { token in
                Defaults[.token] = ":\(token)".base64Encoded!
            }, onError: { error in
                
            }).disposed(by: disposeBag)
        return true
    }


}

extension AppDelegate {
    private func setUpKeyboardManager() {
        let keyboardManager = IQKeyboardManager.shared()
        keyboardManager.isEnabled = true
        keyboardManager.shouldResignOnTouchOutside = true
    }
}

