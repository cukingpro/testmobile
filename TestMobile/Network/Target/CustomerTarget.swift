//
//  CustomerTarget.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import Moya

enum FieldType: String, CaseIterable {
    case name = "nom"
    case email = "email"
    case phone = "tel"
    case address = "adresse"
    case postalCode = "code_postal"
    case city = "ville"
}

enum CustomerTarget {

    case auth

    case getCustomerList(fields: [FieldType])

    case getCustomer(id: String)

    case editCustomer(id: String)

}

extension CustomerTarget: TargetType, AccessTokenAuthorizable {

    var baseURL: URL {
        return URL(string: "https://evaluation-technique.lundimatin.biz/api")!
    }

    var path: String {
        switch self {
        case .auth:
            return "auth"
        case .getCustomerList:
            return "clients"
        case .getCustomer:
            return "clients/id"
        case .editCustomer:
            return "clients/id"
        }
    }

    var method: Method {
        switch self {
        case .auth:
            return .post
        case .getCustomerList:
            return .get
        case .getCustomer:
            return .get
        case .editCustomer:
            return .put
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .auth:
            let parametes: [String: Any] = [
                "username": "test_api",
                "password": "api123456",
                "password_type": 0,
                "code_application": "webservice_externe",
                "code_version": "1"
            ]
            return .requestParameters(parameters: parametes, encoding: JSONEncoding.default)
            
        case .getCustomerList(let fields):
            let fieldsString = fields.map({ $0.rawValue }).joined(separator: ",")
            let params = [
                "fields": fieldsString
            ]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            
        default:
            return .requestPlain
        }
    }

    var headers: [String : String]? {
        return [
            "Accept": "application/api.rest-v1+json",
            "Content-Type": "application/json",
        ]
    }

    var authorizationType: AuthorizationType {
        switch self {
        case .auth:
            return .none
        default:
            return .basic
        }
    }

}
