//
//  CustomerService.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import Moya
import SwiftyUserDefaults
import RxSwift
import RxCocoa
import ObjectMapper
import Moya_ObjectMapper

typealias JSONObject = [String: Any]
typealias JSONArray = [JSONObject]

struct DefaultParameters {
    static let fields = FieldType.allCases
}

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

final class CustomerService {

    private static let provider = MoyaProvider<CustomerTarget>(
        plugins: [
            NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter),
            AccessTokenPlugin(tokenClosure: Defaults[.token])
        ]
    )

    static func logIn() -> Single<String> {
        return provider.rx.request(.auth).mapJSON().map({ json -> String in
            guard
                let json = json as? JSONObject,
                let datas = json["datas"] as? JSONObject,
                let token = datas["token"] as? String
            else {
                throw NSError(domain: "", code: 1, userInfo: nil)
            }
            return token
        })
    }
    
    static func getCustomer(fields: [FieldType] = DefaultParameters.fields) -> Single<[Customer]> {
        return provider
            .rx
            .request(.getCustomerList(fields: fields))
            .mapArray(Customer.self, atKeyPath: "datas")
    }
}
