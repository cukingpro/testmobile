//
//  DefaultKeys.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import SwiftyUserDefaults

extension DefaultsKeys {
    static let token = DefaultsKey<String>("token")
}
