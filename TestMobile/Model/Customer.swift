//
//  Customer.swift
//  TestMobile
//
//  Created by Huy Tong on 11/9/18.
//  Copyright © 2018 Huy Tong. All rights reserved.
//

import ObjectMapper

final class Customer: Mappable {

    var name: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var address: String?
    var postalCode: String?
    var city: String?
    var phone: String?

    init?(map: Map) {}
    
    init() {}

    func mapping(map: Map) {
        name <- map["nom"]
        if let names = name?.components(separatedBy: " ") {
            firstName = names.first
            lastName = names.last
        }
        email <- map["email"]
        address <- map["adresse"]
        postalCode <- map["code_postal"]
        city <- map["ville"]
        phone <- map["tel"]
    }
}
